#include <iostream>
using namespace std;

void printArray(float arr[], int size) {
    for (int i = 0; i < size; i++) {
        cout << "arr[" << i << "]: " << arr[i] << endl;
    }
}

int countTarget(float arr[], int size, float target) {
    int count = 0;

    for (int i = 0; i < size; i++) {
        if (arr[i] == target) {
            count++;
        }
    }

    return count;
}

int countGreaterThanTarget(float arr[], int size, float target) {
    int count = 0;

    for (int i = 0; i < size; i++) {
        if (arr[i] > target) {
            count++;
        }
    }

    return count;
}

int countLessThanTarget(float arr[], int size, float target) {
    int count = 0;

    for (int i = 0; i < size; i++) {
        if (arr[i] < target) {
            count++;
        }
    }

    return count;
}

int countInRange(float arr[], int size, float min, float max) {
    int count = 0;

    for (int i = 0; i < size; i++) {
        if (arr[i] >= min && arr[i] <= max) {
            count++;
        }
    }

    return count;
}

float sumOfArray(float arr[], int size) {
    float total = 0;

    for (int i = 0; i < size; i++) {
        // total = total + arr[i]
        total += arr[i];
    }

    return total;
}

int countConsecutiveOccurrences(float arr[], int size, float target) {
    int count = 0;
    int max = 0;

    for (int i = 0; i < size; i++) {
        if (arr[i] == target) {
            count++;
        } else {
            if (count > max) {
                max = count;
            }
            count = 0;
        }
    }

    if (count > max) {
        max = count;
    }

    return max;
}

int main() {

    /*
        Data we want to represent: daily rainfall (in inches):

        Sun: 0.00 in
        Mon: 0.21 in
        Tue: 0.37 in
        Wed: 0.48 in
        Thu: 0.00 in
        Fri: 0.00 in
        Sat: 0.00 in

        Questions we want to answer:

        How many days did we have without rain? 4
        How many days did we have with rain? 3
        How many days did it rain less than 0.3 inches? 5
        How many days did it rain between 0.4 and 0.6 inches? 1
        How much rainfall did we get in total? 1.06
        What was the longest dry period? 3
    */

    int size = 7;
    float rainfall[] = {0.0, 0.21, 0.37, 0.48, 0.0, 0.0, 0.0};

    cout << "How many days did we have without rain? " << countTarget(rainfall, size, 0.0) << endl;
    cout << "How many days did we have with rain? " << countGreaterThanTarget(rainfall, size, 0.0) << endl;
    cout << "How many days did it rain less than 0.3 inches? " << countLessThanTarget(rainfall, size, 0.3) << endl;
    cout << "How many days did it rain between 0.4 and 0.6 inches? " << countInRange(rainfall, size, 0.4, 0.6) << endl;
    cout << "How much rainfall did we get in total? " << sumOfArray(rainfall, size) << endl;
    cout << "What was the longest dry period? " << countConsecutiveOccurrences(rainfall, size, 0.0) << endl;

    return 0;
}