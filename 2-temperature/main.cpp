#include <iostream>
using namespace std;

float findMaxValue(float arr[], int size) {
    float max = arr[0];

    for (int i = 1; i < size; i++) {
        if (arr[i] > max) {
            max = arr[i];
        }
    }

    return max;
}

float findMaxValueIndex(float arr[], int size) {
    float max = arr[0];
    int maxIndex = 0;

    for (int i = 1; i < size; i++) {
        if (arr[i] > max) {
            max = arr[i];
            maxIndex = i;
        }
    }

    return maxIndex;
}

float findMinValue(float arr[], int size) {
    float min = arr[0];

    for (int i = 1; i < size; i++) {
        if (arr[i] < min) {
            min = arr[i];
        }
    }

    return min;
}

float findMinValueIndex(float arr[], int size) {
    float min = arr[0];
    int minIndex = 0;

    for (int i = 1; i < size; i++) {
        if (arr[i] < min) {
            min = arr[i];
            minIndex = i;
        }
    }

    return minIndex;
}

void findHottestDay(float temperatures[], string dayOfWeek[], int size) {
    int hottestDayIndex = findMaxValueIndex(temperatures, size);
    cout << "The hottest day was " << dayOfWeek[hottestDayIndex % 7] << " with a temperature of " << temperatures[hottestDayIndex] << "." << endl;
}

void findColdestDay(float temperatures[], string dayOfWeek[], int size) {
    int coldestDayIndex = findMinValueIndex(temperatures, size);
    cout << "The coldest day was " << dayOfWeek[coldestDayIndex % 7] << " with a temperature of " << temperatures[coldestDayIndex] << "." << endl;
}

int main() {

    /*
        Data we want to represent: average daily temperatures (in Fahrenheit)
        
        Sun: 78.9 F
        Mon: 74.8 F
        Tue: 71.7 F
        Wed: 78.8 F
        Thu: 72.2 F
        Fri: 75.1 F
        Sat: 84.4 F

        Questions we want to answer:

        What the hottest day of the week?
        The hottest day was Saturday with a temperature of 84.4.

        What the coldest day of the week?
        The coldest day was Tuesday with a temperature of 71.7.

        Generate 4 weeks worth of data with Mockaroo and answer same questions.
    */

    // int size = 7;
    // float temperatures[] = {78.9, 74.8, 71.7, 78.8, 72.2, 75.1, 84.4};
    int size = 28;
    float temperatures[] = {83.57, 94.04, 83.77, 82.18, 98.89, 93.66, 90.97, 105.36, 88.36, 108.01, 88.63, 97.21, 83.96, 105.63, 88.16, 101.66, 98.27, 80.14, 90.58, 104.51, 103.78, 86.69, 94.49, 87.35, 84.27, 101.98, 91.05, 95.69};
    string dayOfWeek[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

    // cout << "Max Value: " << findMaxValue(temperatures, size) << endl;
    // cout << "Min Value: " << findMinValue(temperatures, size) << endl;

    // cout << "The hottest day was " << dayOfWeek[6] << " with a temperature of " << temperatures[6] << "." << endl;
    // cout << "The coldest day was " << dayOfWeek[2] << " with a temperature of " << temperatures[2] << "." << endl;

    // cout << "Min Index: " << findMinValueIndex(temperatures, size) << endl;
    // cout << "Max Index: " << findMaxValueIndex(temperatures, size) << endl;

    findHottestDay(temperatures, dayOfWeek, size);
    findColdestDay(temperatures, dayOfWeek, size);

    return 0;
}